# pdf图片提取测试代码

提取pdf中的图片保存下来

## 引用的开源库

1. jai-imageio-jpeg2000   java操作图片工具
2. thumbnailator          github开源的图片处理工具
3. itextpdf               pdf编辑操作的库
4. pdfbox                 另一种pdf编辑操作的库